#include <QtCore>

#include <signal.h>

int main(int argc, char* argv[])
{
    QCoreApplication application(argc, argv);

    QTimer tick;
    tick.start(1000);
    QObject::connect(&tick, &QTimer::timeout, []{ qDebug("Tick."); });

    QTimer::singleShot(5000, &application, []{ raise(SIGTRAP); });

    QTimer::singleShot(10000, &application, &QCoreApplication::quit);
    return application.exec();
}
