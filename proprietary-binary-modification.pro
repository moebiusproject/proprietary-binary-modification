TEMPLATE = subdirs
SUBDIRS = \
    00-samples-with-different-signatures \
    01-raising-trap-coredumps \
    02-handle-sigtrap \
    03-catch-function \
