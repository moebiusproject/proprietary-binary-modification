#include <QtCore>

#include <signal.h>

static void handler(int signal)
{
    qDebug() << "Got signal" << signal;
}

int main(int argc, char* argv[])
{
    QCoreApplication application(argc, argv);

    QTimer tick;
    tick.start(1000);
    QObject::connect(&tick, &QTimer::timeout, []{ qDebug("Tick."); });

    QTimer::singleShot(5000, &application, []{ raise(SIGTRAP); });

    struct sigaction action;

    action.sa_flags = SA_SIGINFO;
    sigemptyset(&action.sa_mask);
    action.sa_handler = handler;
    const int set = sigaction(SIGTRAP, &action, NULL);
    qDebug() << "Set signal handler?" << (set != -1);

    QTimer::singleShot(10000, &application, &QCoreApplication::quit);
    return application.exec();
}
