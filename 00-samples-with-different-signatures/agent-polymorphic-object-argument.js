// An example that checks the type of a polymorphic object by comparing the
// pointer on the object with the address of its alleged vtable. Seems that
// there is a 8 byte difference between them, as part of the Linux 64 bit ABI:
// https://reverseengineering.stackexchange.com/q/27485/33935
// https://itanium-cxx-abi.github.io/cxx-abi/abi.html

const pointerToFunction = Module.findExportByName(null, "_Z32functionVoidPolymorphicParameterP9BaseClass");
const vtable = Module.findExportByName(null, "_ZTV12DerivedClass");

Interceptor.attach(pointerToFunction, {
    onEnter: function(args) {
        const pObject = args[0];
        console.log("Object pointer:", pObject);
        console.log("Objects's vtable:", pObject.readPointer());
        console.log("vtable symbol", vtable);
        console.log("difference", pObject.readPointer().sub(vtable));
    },
});

