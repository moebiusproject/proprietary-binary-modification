#include <QtCore>

struct Struct1
{
    int x;
};

struct Struct2
{
    int x;
    int y;
};

////////////////////////////////////////////////////////////////////////////////

struct CStringData
{
    qint64 nRefs;
    qint32 nDataLength;
    qint32 nAllocLength;
};

struct CString
{
    CStringData* m_pchData;
};


////////////////////////////////////////////////////////////////////////////////

void functionVoid0()
{
    qDebug() << Q_FUNC_INFO;
}

void functionVoid1(int* x)
{
    qDebug() << Q_FUNC_INFO << x;
}

void functionVoid2(int x)
{
    qDebug() << Q_FUNC_INFO << x;
}

void functionVoid3(Struct1 x)
{
    qDebug() << Q_FUNC_INFO << x.x;
}

void functionVoid4(Struct2 x)
{
    qDebug() << Q_FUNC_INFO << x.x << x.y;
}


////////////////////////////////////////////////////////////////////////////////

struct BaseClass {
    virtual bool someVirtualFunction() { return false; }
};
struct DerivedClass : public BaseClass {
    bool someVirtualFunction() override { return true; }
};

class FilterItAll : public QObject
{
    bool eventFilter(QObject* watched, QEvent* event) override
    {
        Q_UNUSED(watched);
        Q_UNUSED(event);
        return true;
    }
};

void functionVoidPolymorphicParameter(QObject* object)
{
    const bool value = object->eventFilter(nullptr, nullptr);
    qDebug() << value;
}

void functionVoidPolymorphicParameter(BaseClass* object)
{
    qDebug() << object->someVirtualFunction();
}

////////////////////////////////////////////////////////////////////////////////

QString functionComplex1()
{
    return QString::fromLatin1("hello");
}


int main(int argc, char* argv[])
{
    QCoreApplication application(argc, argv);

    QTimer tick;
    tick.start(1000);
//    QObject::connect(&tick, &QTimer::timeout, []{ qDebug() << functionComplex1(); });
    QObject::connect(&tick, &QTimer::timeout, []{
        // FilterItAll object;
        DerivedClass object;
        functionVoidPolymorphicParameter(&object);
    });

    QTimer::singleShot(500000, &application, &QCoreApplication::quit);
    return application.exec();
}
