#include <stdio.h>
#include <dlfcn.h>

__attribute__((constructor))
static void initialize() {
    printf("Symbol (NEXT): %p\n", dlsym(RTLD_NEXT,    "message"));
    printf("Symbol (DFLT): %p\n", dlsym(RTLD_DEFAULT, "message"));
}

// gcc -shared -fPIC -ldl -o inject.so inject.c
