// Note to self: we can sort it later by function (for plotting, etc.) with
// `sort -k2 -t, UtilRandInt.csv`
let logger = new File("UtilRandInt.csv", "a");

const utilRandIntPointer =
    Process.platform == "windows" ?
    DebugSymbol.getFunctionByName("CUtil::UtilRandInt") :
    Module.findExportByName(null, "_ZN5CUtil11UtilRandIntEll");
Interceptor.attach(utilRandIntPointer, {
    onEnter: function(args) {
        this.sides = args[0].toInt32();
        this.luck  = args[1].toInt32();
        // console.log("CSV:", args[0].toInt32(), args[1].toInt32());
        this.caller = Thread.backtrace(this.context, Backtracer.ACCURATE)
                            .map(DebugSymbol.fromAddress).join('\n');
    },
    onLeave: function(retval) {
        logger.write(
            new Date().toISOString() + "," +
            this.caller + "," +
            this.sides + "," +
            this.luck + "," +
            retval.toInt32() + "\n");
        // console.log("CSV:", this.caller, this.sides, this.luck, retval.toInt32());
        logger.flush();
    },
});
