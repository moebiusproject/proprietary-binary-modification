//
// IMPORTANT. This is just a work in progress! It doesn't work.
//

const functionPointer = Module.findExportByName(null, "_ZN11CGameSprite6DamageEP5CItemS1_iiRK13CAIObjectTypessPS_i")

Interceptor.attach(functionPointer.add("0x750"), function() {
    console.log("[*] Point0 happenned. Backstab decided to be '0'");
});

Interceptor.attach(functionPointer.add("0x753"), function() {
    console.log("[*] Facestab for everyone?");
    this.context.r15d = 3;
});


// Seems to be called:
// 1. On a successful sneak attack from the back.
// Result is one at our level.

// iVar15 = CRuleTables::GetSneakAttack(pCVar4,pCVar1,uVar26);
// if (iVar15 < 1) goto LAB_00906080;

Interceptor.attach(functionPointer.add("0x6BF"), function() {
    console.log("[*] Point1 happenned: GetSneakAttack called. Result:", this.context.rax);
    // console.log('Context  : ' + JSON.stringify(this.context));
});


// Weird, but it's before the check that seems familiar with what Bubb pointed
// if (*(short *)(pCVar29 + 0x8c) < 2) goto LAB_00906080;
// 1. Reached the point in a successfuly backstab
// 2. Reached the point in a facing attack from stealth
// 3. Did NOT reach the point with sneak attack activated.

Interceptor.attach(functionPointer.add("0x953"), function() {
    console.log("[*] Point2 happenned.");
    // console.log('Context  : ' + JSON.stringify(this.context));
});


