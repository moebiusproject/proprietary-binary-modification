const versionStringPointer = Process.platform == "windows" ?
                             DebugSymbol.fromName("CChitin::GetVersionString").address :
                             Module.findExportByName(null, "_ZN7CChitin16GetVersionStringEv");
// const versionString = new NativeFunction(versionStringPointer, 'pointer', []);
Interceptor.attach(versionStringPointer, {
    onLeave(retval) {
        console.log("[*] Intercepted by Frida");
        console.log("[*] retval ...=", retval.readPointer().readCString());
        let heap = retval.readPointer();
        heap.writeUtf8String("v2.5 Injected by Frida :)");
    }
});
