const volumePointer = Process.platform == "windows" ?
                      DebugSymbol.getFunctionByName("CSoundMixer::SetGlobalVolume") :
                      Module.findExportByName(null, "_ZN11CSoundMixer15SetGlobalVolumeEl");
Interceptor.attach(volumePointer, {
    onEnter: function(args) {
        // On Windows 32 bit it's a __thiscall function. The `this` pointer is
        // in the ECX register, and the passed volume is in Stack[0x4] so frida
        // gets it through the "first" arg.
        // On Linux 64 bit `this` is the 0th argument, so the volume is the 1st.
        const index = Process.platform == "windows" ? 0 : 1;
        args[index] = ptr("100");
    },
});

