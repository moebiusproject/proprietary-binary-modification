let logger = new File("CombatLog.txt", "a");
logger.write(`\n------------------------------ New start\n`);

// Old approach. Worked on 2.5, but not on 2.6, because some change that it will
// be interesting to find out (section of the binary where the symbols are?).
// const pAddMessage = Module.findExportByName(null, "_ZN15CMessageHandler10AddMessageEP8CMessagei");

const pAddMessage = DebugSymbol.getFunctionByName("_ZN15CMessageHandler10AddMessageEP8CMessagei");

const vtableCMessageDisplayText = Module.findExportByName(null, "_ZTV19CMessageDisplayText");
const vtableCMessageDisplayTextRef = Module.findExportByName(null, "_ZTV22CMessageDisplayTextRef");

class CString {
    constructor(address) {
        // console.log("String at", address, address.readPointer().readCString());
        this._address = address;
        // TODO. Consider if we want to read the value right away, to avoid a
        // dangling pointer. It would increase the memory, though. And would not
        // allow to write if we want to use the wrapper to write in the engine.
        // this._text = address.readPointer().readCString();
    }

    toString() {
        return this._address.readPointer().readCString();
    }
}

class CMessage {
    constructor(address) {
        this.address = address;

        // Start skipping the vtable pointer.
        let pointer = address.add(Process.pointerSize);

        this._targetId = pointer.readS32();
        pointer = pointer.add(4);

        this._sourceId = pointer.readS32();
        pointer = pointer.add(4);

        this._size = pointer.sub(this.address);
    }

    get targetId() {
        return this._targetId;
    }

    get sourceId() {
        return this._sourceId;
    }

    get size() {
        return this._size;
    }

    toString() {
        return `CMessage(targetId=${this.targetId}, sourceId=${this.sourceId})`;
    }
}

class CMessageDisplayText extends CMessage {
    constructor(address) {
        super(address);
        let pointer = address.add(super.size);

        this._name = new CString(pointer);
        pointer = pointer.add(Process.pointerSize);

        this._text = new CString(pointer);
        pointer = pointer.add(Process.pointerSize);

        this._nameColor = pointer.readU32();
        pointer = pointer.add(4);

        this._textColor = pointer.readU32();
        pointer = pointer.add(4);
    }

    get name() { return this._name; }
    get text() { return this._text; }

    toString() {
        return `CMessageDisplayText(${super.toString()}, `
        + `name="${this._name.toString()}", text="${this._text.toString()}", `
        + `nameColor=${this._nameColor}, textColor=${this._textColor})`;
    }
}


class CMessageDisplayTextRef extends CMessage {
    constructor(address) {
        super(address);
        let pointer = address.add(super.size);

        // No idea what this are... strref? I tried several things to figure out
        // if are pointers to strings, but it did not seem to me.
        this._name = pointer.readU32();
        pointer = pointer.add(4);
        this._text = pointer.readU32();
        pointer = pointer.add(4);

        this._nameColor = pointer.readU32();
        pointer = pointer.add(4);
        this._textColor = pointer.readU32();
        pointer = pointer.add(4);
    }

    // TODO: Figure out what this actually are, and probably find the string as
    // an STRREF?
    get name() { return this._name; }
    get text() { return this._text; }

    toString() {
        return `CMessageDisplayTextRef(${super.toString()}, `
        + `name=${this._name}, text=${this._text}, `
        + `nameColor=${this._nameColor}, textColor=${this._textColor})`;
    }
}

Interceptor.attach(pAddMessage, {
    onEnter(args) {

        // To filter out some noise when outputting traces on each call to
        // AddMessage, I typically discarded some callers via backtrace. I could
        // not notice any performance hit.

        // const callerPtr = Thread.backtrace(this.context, Backtracer.ACCURATE);
        // const caller = DebugSymbol.fromAddress(callerPtr[0]);
        // if (!RegExp("CGameSprite").test(caller))
        //     return;
        // if (RegExp("AIUpdate").test(caller))
        //     return;
        // if (RegExp("ProcessAI").test(caller))
        //     return;
        // if (RegExp("Multiplayer").test(caller))
        //     return;
        // if (RegExp("ProcessEffectList").test(caller))
        //     return;
        // if (RegExp("CheckModal").test(caller))
        //     return;

        // if (!RegExp("CGameSprite.*Hit").test(caller))
        //     return;

        // console.log("\n[*] AddMessage from", caller.address, caller.name);

        // args[0] is the `CMessageHandler* this` of the member function.
        // The 1st parameter is the CMessage*, so the args[1].
        const pCMessage = args[1];
        const thisVtable = pCMessage.readPointer();

        let message = null;

        if (thisVtable.equals(vtableCMessageDisplayText.add(0x10))) {
            message = new CMessageDisplayText(pCMessage);
        }
        else if (thisVtable.equals(vtableCMessageDisplayTextRef.add(0x10))) {
            message = new CMessageDisplayTextRef(pCMessage);
        }

        if (!message)
            return;
        if (message instanceof CMessageDisplayTextRef) // We can't print them yet
            return;

        // console.log("[*] AddMessage from", caller.address, caller.name);
        // console.log(thisVtable.sub(vtableCMessageDisplayText));

        // const varCMessage = new CMessage(pCMessage);
        // console.log("|| CMessage!", varCMessage);

        console.log("[*] message", message);
        logger.write(`${message.name}: ${message.text}\n`);
        logger.flush();
    }
});



// I don't do anything below this lines. I just keep it for reference on how
// reading data can be done also more manually and in another location (here is
// the destructor of the object that I had interest on, though careful, as there
// are two versions of a destructor in some cases). Data is read "manually", as
// I've seen in examples. Just have a pointer, read the value, advance the
// pointer and advance again till you've read it all. No need for classes if you
// don't want something structured.

// const messageDtor = Module.findExportByName(null, "_ZN19CMessageDisplayTextD0Ev");
const messageDtor = Module.findExportByName(null, "_ZN22CMessageDisplayTextRefD0Ev");


Interceptor.attach(messageDtor,
    function (args) {
        return;

        const pCMessage = args[0];

        // console.log(pCMessage.add(0x10).readPointer().readCString(),
        //             pCMessage.add(0x18).readPointer().readCString())


        // Skip the vtable
        let p = pCMessage.add(Process.pointerSize);

        // I was following EEex docs, but those are for 32 bit, and I did not
        // know for sure the size of the fields which are not pointers.
        let small = true; // small? size==4. Not small? size==8.

        const targetId = small ? p.readS32() : p.readS64();
        p = p.add(small ? 4 : 8);
        const sourceId = small ? p.readS32() : p.readS64();
        p = p.add(small ? 4 : 8);

        // Now we've read the CMessage base fields. Go to the CString.
        const name = p.readPointer().readCString();
        p = p.add(Process.pointerSize);
        const message = p.readPointer().readCString();
        // console.log(name + ': ' + message);
        p = pCMessage.add(Process.pointerSize);

        // Unsigned! The previous were signed
        const nameColor = small ? p.readU32() : p.readU64();
        p = p.add(small ? 4 : 8);
        const textColor = small ? p.readU32() : p.readU64();
        p = p.add(small ? 4 : 8);
        console.log("Name color", nameColor, "Text color", textColor);
    }
);
